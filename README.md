# Ansible Role Baseline
An Ansible role to secure an Ubuntu machine as a baseline. For use with Ansible Galaxy.

## Tasks
- Bootstrap: Ensure all packages are up to date
- Firewall: Ensures firewall is installed
- fail2ban: Ensure is installed
- NCSC: Recommended security guidance for [Ubuntu 18.04](https://www.ncsc.gov.uk/guidance/eud-security-guidance-ubuntu-1804-lts)

## Example Playbook
An example of how to use the role:

```yaml
- hosts: all
  remote_user: root
  tasks:
  - include_role:
      name: baseline
```

## License
AGPLv3
